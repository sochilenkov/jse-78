package ru.t1.sochilenkov.tm.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.sochilenkov.tm.model.User;

@Repository
public interface IUserRepository extends JpaRepository<User, String> {

    User findByLogin(final String login);

}
